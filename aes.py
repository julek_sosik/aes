sbox = [
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, 
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, 
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, 
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, 
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, 
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, 
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, 
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16, 
]

invsbox = [
    0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb, 
    0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb, 
    0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e, 
    0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25, 
    0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92, 
    0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84, 
    0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06, 
    0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b, 
    0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73, 
    0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e, 
    0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b, 
    0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4, 
    0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f, 
    0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef, 
    0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61, 
    0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d, 
]

rcon = [
    0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 
    0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 
    0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 
    0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 
    0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 
    0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 
    0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 
    0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 
    0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 
    0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 
    0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 
    0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 
    0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 
    0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 
    0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 
    0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d
]


class AES:
    def __init__(self, key, IV, mode):
        _key = bytearray(key)
        _IV = bytearray(IV)
        assert len(_key) in [16, 24, 32], 'Length of key must be 128, 192, or 256. Provided key length: {}'.format(len(_key) * 8)
        if mode != 'ECB':
            assert len(_IV) == 16, 'Length of IV must be 128. Provided IV length: {}'.format(len(IV) * 8)
        assert mode in ['ECB', 'CBC', 'CFB', 'OFB', 'CTR'], 'Ciphering modes supported: \'ECB\', \'CBC\', \'CFB\', \'OFB\', \'CTR\''
        self.key = _key
        self.IV = _IV
        self.mode = mode
        self.cycles = {16: 10, 24: 12, 32: 14}[len(_key)]
        self.keys = rijndaelKeySchedule(_key)
    
    def encrypt(self, plaintext):
        _plaintext = bytearray(plaintext)
        assert len(_plaintext) % 16 == 0, 'AES only accepts blocks of 128 bits'
        assert len(_plaintext) != 0, 'AES cannot encrypt 0 bytes'
        
        plain_text_blocks = [_plaintext[i:i+16] for i in range(0, len(_plaintext), 16)]
        
        cipher_text = bytearray()
        if self.mode == 'ECB':
            for block in plain_text_blocks:
                cipher_text += self.encryptBlock(block)
        elif self.mode == 'CBC':
            for i in range(16):
                plain_text_blocks[0][i] ^= self.IV[i]
            cipher_text += self.encryptBlock(plain_text_blocks[0])

            for i in range(len(plain_text_blocks)-1):
                for j in range(16):
                    plain_text_blocks[i+1][j] ^= cipher_text[(i*16)+j]
                cipher_text += self.encryptBlock(plain_text_blocks[i+1])
        elif self.mode == 'CFB':
            intermediate_text = self.encryptBlock(self.IV)
            for i in range(16):
                intermediate_text[i] ^= plain_text_blocks[0][i]
            cipher_text += intermediate_text

            for i in range(len(plain_text_blocks)-1):
                intermediate_text = self.encryptBlock(intermediate_text)
                for j in range(16):
                    intermediate_text[j] ^= plain_text_blocks[i+1][j]
                cipher_text += intermediate_text
        elif self.mode == 'OFB':
            intermediate_text = self.encryptBlock(self.IV)
            cipher_text += bytearray(16)
            for i in range(16):
                cipher_text[i] = intermediate_text[i] ^ plain_text_blocks[0][i]

            for i in range(len(plain_text_blocks)-1):
                intermediate_text = self.encryptBlock(intermediate_text)
                cipher_text += bytearray(16)
                for j in range(16):
                    cipher_text[(i+1)*16+j] = intermediate_text[j] ^ plain_text_blocks[i+1][j]
        elif self.mode == 'CTR':
            counter = bytes_to_int(self.IV) % 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF  # keep it to 128 bits
            for block in plain_text_blocks:
                intermediate_text = self.encryptBlock(int_to_bytes(counter))
                for i in range(16):
                    intermediate_text[i] ^= block[i]
                cipher_text += intermediate_text
                counter = (counter + 1) % 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF  # keep it to 128 bits

        return cipher_text
        
    def encryptBlock(self, block):
        _block = bytearray(block)
        
        # init round
        _block = self.addRoundKey(_block, 0)
        
        # rounds
        for i in range(self.cycles - 1):
            _block = self.subBytes(_block)
            _block = self.shiftRows(_block)
            _block = self.mixColumns(_block)
            _block = self.addRoundKey(_block, i+1)
        
        # final round
        _block = self.subBytes(_block)
        _block = self.shiftRows(_block)
        _block = self.addRoundKey(_block, self.cycles)
        
        return _block

    def decrypt(self, cipher_text):
        _cipher_text = bytearray(cipher_text)
        assert len(_cipher_text) % 16 == 0, 'Cipher is wrong length'
        assert len(_cipher_text) != 0, 'AES cannot decrypt 0 bytes'

        cipher_text_blocks = [_cipher_text[i:i+16] for i in range(0, len(_cipher_text), 16)]

        plaintext = bytearray()
        if self.mode == 'ECB':
            for block in cipher_text_blocks:
                plaintext += self.decryptBlock(block)
        elif self.mode == 'CBC':
            intermediate_text = self.decryptBlock(cipher_text_blocks[0])
            for i in range(16):
                intermediate_text[i] ^= self.IV[i]
            plaintext += intermediate_text

            for i in range(len(cipher_text_blocks)-1):
                intermediate_text = self.decryptBlock(cipher_text_blocks[i+1])
                for j in range(16):
                    intermediate_text[j] ^= cipher_text_blocks[i][j]
                plaintext += intermediate_text
        elif self.mode == 'CFB':
            intermediate_text = self.encryptBlock(self.IV)
            for i in range(16):
                intermediate_text[i] ^= cipher_text_blocks[0][i]
            plaintext += intermediate_text

            for i in range(len(cipher_text_blocks)-1):
                intermediate_text = self.encryptBlock(cipher_text_blocks[i])
                for j in range(16):
                    intermediate_text[j] ^= cipher_text_blocks[i+1][j]
                plaintext += intermediate_text
        elif self.mode == 'OFB':
            intermediate_text = self.encryptBlock(self.IV)
            plaintext += bytearray(16)
            for i in range(16):
                plaintext[i] = intermediate_text[i] ^ cipher_text_blocks[0][i]

            for i in range(len(cipher_text_blocks)-1):
                intermediate_text = self.encryptBlock(intermediate_text)
                plaintext += bytearray(16)
                for j in range(16):
                    plaintext[(i+1)*16+j] = intermediate_text[j] ^ cipher_text_blocks[i+1][j]
        elif self.mode == 'CTR':
            counter = bytes_to_int(self.IV) % 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF  # keep it to 128 bits
            for block in cipher_text_blocks:
                intermediate_text = self.encryptBlock(int_to_bytes(counter))
                for i in range(16):
                    intermediate_text[i] ^= block[i]
                plaintext += intermediate_text
                counter = (counter + 1) % 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF  # keep it to 128 bits

        return plaintext

    def decryptBlock(self, block):
        _block = bytearray(block)

        # init round
        _block = self.addRoundKey(_block, self.cycles)

        # rounds
        for i in range(self.cycles - 1, 0, -1):
            _block = self.invShiftRows(_block)
            _block = self.invSubBytes(_block)
            _block = self.addRoundKey(_block, i)
            _block = self.invMixColumns(_block)

        # final round
        _block = self.invShiftRows(_block)
        _block = self.invSubBytes(_block)
        _block = self.addRoundKey(_block, 0)

        return _block
    
    def addRoundKey(self, block, round):
        _block = bytearray(block)
        for i in range(16):
            _block[i] = _block[i] ^ self.keys[(round*16)+i]
        return _block
    
    def subBytes(self, block):
        _block = bytearray(16)
        for i in range(len(block)):
            _block[i] = sbox[block[i]]
        return _block

    def invSubBytes(self, block):
        _block = bytearray(16)
        for i in range(len(block)):
            _block[i] = invsbox[block[i]]
        return _block
    
    def shiftRows(self, block):
        _block = bytearray(16)
        _block[0] = block[0]
        _block[4] = block[4]
        _block[8] = block[8]
        _block[12] = block[12]

        _block[1] = block[5]
        _block[5] = block[9]
        _block[9] = block[13]
        _block[13] = block[1]

        _block[2] = block[10]
        _block[6] = block[14]
        _block[10] = block[2]
        _block[14] = block[6]

        _block[3] = block[15]
        _block[7] = block[3]
        _block[11] = block[7]
        _block[15] = block[11]

        return _block

    def invShiftRows(self, block):
        _block = bytearray(16)
        _block[0] = block[0]
        _block[4] = block[4]
        _block[8] = block[8]
        _block[12] = block[12]

        _block[5] = block[1]
        _block[9] = block[5]
        _block[13] = block[9]
        _block[1] = block[13]

        _block[10] = block[2]
        _block[14] = block[6]
        _block[2] = block[10]
        _block[6] = block[14]

        _block[15] = block[3]
        _block[3] = block[7]
        _block[7] = block[11]
        _block[11] = block[15]

        return _block
    
    def mixColumns(self, block):
        b = bytearray(4)
        _block = bytearray(block)
        for i in range(4):
            v = block[i*4:(i*4)+4]
            b[0] = gmul(2, v[0]) ^ gmul(3, v[1]) ^ gmul(1, v[2]) ^ gmul(1, v[3])
            b[1] = gmul(1, v[0]) ^ gmul(2, v[1]) ^ gmul(3, v[2]) ^ gmul(1, v[3])
            b[2] = gmul(1, v[0]) ^ gmul(1, v[1]) ^ gmul(2, v[2]) ^ gmul(3, v[3])
            b[3] = gmul(3, v[0]) ^ gmul(1, v[1]) ^ gmul(1, v[2]) ^ gmul(2, v[3])
            _block[i*4:(i*4)+4] = b

        return _block

    def invMixColumns(self, block):
        b = bytearray(4)
        _block = bytearray(block)
        for i in range(4):
            v = block[i*4:(i*4)+4]
            b[0] = gmul(14, v[0]) ^ gmul(11, v[1]) ^ gmul(13, v[2]) ^ gmul(9, v[3])
            b[1] = gmul(9, v[0]) ^ gmul(14, v[1]) ^ gmul(11, v[2]) ^ gmul(13, v[3])
            b[2] = gmul(13, v[0]) ^ gmul(9, v[1]) ^ gmul(14, v[2]) ^ gmul(11, v[3])
            b[3] = gmul(11, v[0]) ^ gmul(13, v[1]) ^ gmul(9, v[2]) ^ gmul(14, v[3])
            _block[i*4:(i*4)+4] = b

        return _block
        

def gmul(a, b):
    p = 0
    while b:
        if b & 1:
            p ^= a
        
        if a & 0x80:
            a = (a << 1) ^ 0x11b
        else:
            a <<= 1
        b >>= 1
    return p


def rijndaelKeySchedule(key):
    def core(_4_bytes, rcon_i):
        ret = bytearray(_4_bytes)
        ret = rotateLeft(ret, 1)
        for j in range(len(ret)):
            ret[j] = sbox[ret[j]]
        ret[0] = ret[0] ^ rcon[rcon_i]
        return ret

    n = len(key)
    b = {16: 176, 24: 208, 32: 240}[len(key)]

    expanded_key = bytearray(key)
    
    rcon_i = 1
    
    while len(expanded_key) < b:
        t = expanded_key[-4:]
        t = core(t, rcon_i)
        rcon_i += 1
        for i in range(len(t)):
            t[i] = t[i] ^ expanded_key[-n+i]
        expanded_key += t
        
        for i in range(3):
            t = expanded_key[-4:]
            for j in range(len(t)):
                t[j] = t[j] ^ expanded_key[-n+j]
            expanded_key += t
        
        if len(key) == 32:
            t = expanded_key[-4:]
            for i in range(4):
                t[i] = sbox[t[i]] ^ expanded_key[-n+i]
            expanded_key += t
        
        if len(key) == 16:
            pass
        elif len(key) == 24:
            for i in range(2):
                t = expanded_key[-4:]
                for j in range(len(t)):
                    t[j] ^= expanded_key[-n+j]
                expanded_key += t
        elif len(key) == 32:
            for i in range(3):
                t = expanded_key[-4:]
                for j in range(len(t)):
                    t[j] ^= expanded_key[-n+j]
                expanded_key += t
    return expanded_key


def rotateLeft(l, n):
    return l[n:] + l[:n]


def bytes_to_int(b, endian='little'):
    ret = int()
    if endian == 'little':
        for i in range(len(b)):
            ret += b[i] << i*8
    elif endian == 'big':
        for i in range(len(b)):
            ret += b[-i-1] << i*8
    return ret


def int_to_bytes(i, endian='little'):
    'Converts an int to 16 bytes'
    ret = bytearray(16)
    if endian == 'little':
        for j in range(16):
            ret[j] = (i >> (j*8)) & 0xFF
    elif endian == 'big':
        for j in range(16):
            ret[-j-1] = (i >> (j*8)) & 0xFF
    return ret

